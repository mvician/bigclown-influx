#!/usr/bin/env python3

import yaml
import os

devices_name = 'devices.yml'


def load_devices(devices_file=''):
    if devices_file == "":
        current_directory = os.path.dirname(os.path.realpath(__file__))
        devices_file = os.path.join(current_directory, devices_name)

    print(devices_file)
    with open(devices_file, 'r') as stream:
        try:
            devices = yaml.load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    # print(devices)
    # print(devices["devices"])
    # print(len(devices["devices"]))
    return devices["devices"]


def get_devicesid(devices_file='', devices=None):
    if devices is None:
        devices = load_devices(devices_file)
    devicesid = {}
    for device in devices:
        devicesid[device["id"]] = device
    return devicesid


def get_devicesname(devices_file='', devices=[]):
    if devices is None:
        devices = load_devices(devices_file)
    devicesname = {}
    for device in devices:
        devicesname[device["name"]] = device
    return devicesname
