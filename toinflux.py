#!/usr/bin/env python3

import requests
import json
from influxdb import InfluxDBClient
import configparser
import time
import yaml


class BigClownInflux:
    def __init__(self, configfile, rules, prefix='ext_', section='influx'):
        self.configfile = configfile
        self.section = section
        self.prefix = prefix
        self.rules = rules

        self.sun = {}

        self.nonfloat = ["weather", "weather_description", "concentration"]
        self.replacing = ["%"]

        self.load_config(configfile)

    def load_config(self, configfile):
        print(configfile)
        self.config = configparser.ConfigParser({
            "host": "localhost",
            "port": "8086"})
        self.config.read(configfile)

        self.influx_host = self.config.get('influx', 'host')
        self.influx_port = int(self.config.get('influx', 'port'))
        self.influx_db = self.config.get('influx', 'db')
        self.influx_user = self.config.get('influx', 'user')
        self.influx_password = self.config.get('influx', 'pass')

        print("influx host:", self.influx_host)
        print("influx port:", self.influx_port)
        print("influx db:", self.influx_db)
        print("influx user:", self.influx_user)

        if self.influx_db != "devtest":
            self.influx = InfluxDBClient(self.influx_host,
                                         self.influx_port,
                                         self.influx_user,
                                         self.influx_password,
                                         self.influx_db)

    def get_data(self, url, query):
        response_get = requests.get(url, params=query)
        response = json.loads(str(response_get.content.decode('utf-8')))
        return response

    def get_value(self, response, names):
        for name in names:
            if name in response:
                response = response[name]
            elif name == 0 and len(response) > 0:
                response = response[0]
            else:
                print("get_value -> none", name)
                return None
        return response

    def create_measurement_json(self, measurement_name, value, tags={}):
        if isinstance(value, str):
            print("is string -> replacing",self.replacing)
            for char in self.replacing:
                value = value.replace(char, "")
        if measurement_name not in self.nonfloat:
            try:
                value = float(value)
            except ValueError:
                return None
        print("value for", measurement_name, ":", value)
        measurement = {
            "measurement": self.prefix+measurement_name,
            "tags": tags,
            "fields": {
                "value": value
            }
        }
        return measurement

    def create_measurement(self, measurement_name, response, names, tags={}):
        value = self.get_value(response, names)
        if value is None:
            return None
        if measurement_name in ["sunrise", "sunset"]:
            self.sun[measurement_name] = value
            return None
        measurement = self.create_measurement_json(measurement_name,
                                                   value, tags)
        print(measurement)
        return measurement

    def parse_result(self, response, tags={}):
        self.sun = {}
        json_body = []
        for key, value in self.rules.items():
            print("rule: ", key, value)
            measurement = self.create_measurement(key, response, value, tags)
            if measurement is not None:
                json_body.append(measurement)
        if len(self.sun) == 2:
            current_timestamp = int(time.time())
            sun = 0
            if current_timestamp > self.sun["sunrise"]\
                    and current_timestamp < self.sun["sunset"]:
                sun = 1
            measurement = self.create_measurement_json("sun", sun, tags)
            print("sun:", sun, self.sun)
            json_body.append(measurement)
        if self.influx_db != "devtest":
            self.influx.write_points(json_body)
