#!/usr/bin/env python3
# coding=utf-8

import os
import time
import sys
import configparser
from collections import namedtuple
import aqicn
import toinflux

Coordinate = namedtuple('Coordinate', ['lat', 'lng'])

aqicn_rules = {
    "pm25": ["data", "iaqi", "pm25", "v"],
    "pm10": ["data", "iaqi", "pm10", "v"],
    "o3": ["data", "iaqi", "o3", "v"],
    "no2": ["data", "iaqi", "no2", "v"],
    "so2": ["data", "iaqi", "so2", "v"],
    "co": ["data", "iaqi", "co", "v"],
    "temp": ["data", "iaqi", "t", "v"],
    "wind": ["data", "iaqi", "w", "v"],
    "rain": ["data", "iaqi", "r", "v"],
    "humidity": ["data", "iaqi", "h", "v"],
    "dew": ["data", "iaqi", "d", "v"],
    "pressure": ["data", "iaqi", "p", "v"],
}

if __name__ == '__main__':
    current_directory = os.path.dirname(os.path.realpath(__file__))
    influx_config = os.path.join(current_directory, "influx.ini")

    influx = toinflux.BigClownInflux(influx_config,
                                     aqicn_rules)

    config_file = os.path.join(current_directory, "aqicn.ini")
    config = configparser.ConfigParser()
    config.read(config_file)

    aqicn_api = config.get('aqicn', 'api')
    print("api:", aqicn_api)
    aqicn_all = config.items()
    print(config._sections['aqicn_locations'])

    locations = {}
    for key, value in config._sections['aqicn_locations'].items():
        latlng = value.split(",")
        if len(latlng) == 2:
            locations[key] = Coordinate(latlng[0], latlng[1])
            print(key, "=>", locations[key])
        else:
            print("Warning, skipping", key, value)

    while True:
        for location, gps in locations.items():
            print("---------")
            print("Location:", location, gps)

            api = aqicn.AqicnApi(secret=aqicn_api)
            response = api.get_location_feed(gps)
            # print(response)

            tags = {"location": location, "source": "aqicn"}

            print(str(response).encode('utf-8'))

            influx.parse_result(response, tags)

        print("waiting")
        time.sleep(60)

    sys.exit(0)
