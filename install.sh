#!/bin/bash

set -x

base_path="$(realpath $(dirname $0))"

services=("bc-aqicn" "bc-openweathermap")

for service in ${services[@]}; do
	echo "Installing $service"

	#ln -sf ${base_path}/${service}.py /usr/bin/${service}
	cp ${base_path}/${service}.service /etc/systemd/system/${service}.service
done

systemctl daemon-reload

for service in ${services[@]}; do
	echo "Installing $service"

	systemctl restart $service
	systemctl enable $service
done
