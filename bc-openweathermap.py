#!/usr/bin/env python3

# import requests
# import json
import os
import time
import sys
# from influxdb import InfluxDBClient
import configparser
# import bigclown_influx
import toinflux

openweathermap_rules = {
    "temp": ['main', 'temp'],
    "humidity": ['main', 'humidity'],
    "visibility": ['visibility'],
    "windspeed": ['wind', 'speed'],
    "winddeg": ['wind', 'deg'],
    "windgust": ['wind', 'gust'],
    "pressure": ['main', 'pressure'],
    "weather": ['weather', 0, 'main'],
    "weather_description": ['weather', 0, 'description'],
    "clouds": ['clouds', 'all'],
    "rain": ['rain', '3h'],
    "snow": ['snow', '3h'],
    "sunrise": ['sys', 'sunrise'],
    "sunset": ['sys', 'sunset'],
}

if __name__ == '__main__':
    current_directory = os.path.dirname(os.path.realpath(__file__))
    influx_config = os.path.join(current_directory, "influx.ini")

    influx = toinflux.BigClownInflux(influx_config,
                                     openweathermap_rules)

    config_file = os.path.join(current_directory, "openweathermap.ini")
    config = configparser.ConfigParser({
        "url": "http://api.openweathermap.org/data/2.5/weather",
        "locations": "Prague"})
    config.read(config_file)

    openweathermap_api = config.get('openweathermap', 'api')
    openweathermap_url = config.get('openweathermap', 'url')
    locations = config.get('openweathermap', 'locations').split(",")
    print("api:", openweathermap_api)
    print("url:", openweathermap_url)
    print(len(locations))
    print(locations)

    while True:
        for location in locations:
            print("---------")
            print("Location:", location)
            openweathermap_query = {'q': location,
                                    'units': 'metric',
                                    'APPID': openweathermap_api}
            response = influx.get_data(openweathermap_url,
                                       openweathermap_query)
            tags = {"location": location, "source": "openweathermap"}

            print(str(response).encode('utf-8'))

            influx.parse_result(response, tags)

        time.sleep(60)

    sys.exit(0)
