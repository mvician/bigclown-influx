#!/usr/bin/env python3

# import requests
# import json
import os
import time
import sys
# from influxdb import InfluxDBClient
import configparser
# import bigclown_influx
import toinflux

wunderground_rules = {
    "temp": ["current_observation", "temp_c"],
    "temp_feels": ["current_observation", "feelslike_c"],
    "humidity": ["current_observation", "relative_humidity"],
    "visibility": ["current_observation", "visibility_km"],
    "rain_1h": ["current_observation", "precip_1hr_metric"],
    "rain_today": ["current_observation", "precip_today_metric"],
    "weather": ["current_observation", "weather"],
    "windspeed": ["current_observation", "wind_kph"],
    "windgust": ["current_observation", "wind_gust_kph"],
    "winddeg": ["current_observation", "wind_degrees"],
    "windchill": ["current_observation", "windchill_c"],
    "pressure": ["current_observation", "pressure_mb"],
    "dewpoint": ["current_observation", "dewpoint_c"],
    "uv": ["current_observation", "UV"],
    "heatindex": ["current_observation", "heat_index_c"],
    "solarradiation": ["current_observation", "solarradiation"],
}
# old ones
#    "temp": ['main', 'temp'],
#    "humidity": ['main', 'humidity'],
#    "visibility": ['visibility'],
#    "windspeed": ['wind', 'speed'],
#    "winddeg": ['wind', 'deg'],
#    "windgust": ['wind', 'gust'],
#    "pressure": ['main', 'pressure'],
#    "weather": ['weather', 0, 'main'],
#    "weather_description": ['weather', 0, 'description'],
#    "clouds": ['clouds', 'all'],
#    "rain": ['rain', '3h'],
#    "snow": ['snow', '3h'],
#    "sunrise": ['sys', 'sunrise'],
#    "sunset": ['sys', 'sunset'],


def get_url(url, api, country, city):
    return url+api+"/conditions/q/"+country+"/"+city+".json"


if __name__ == '__main__':
    current_directory = os.path.dirname(os.path.realpath(__file__))
    influx_config = os.path.join(current_directory, "influx.ini")

    influx = toinflux.BigClownInflux(influx_config,
                                     wunderground_rules)

    config_file = os.path.join(current_directory, "wunderground.ini")
    config = configparser.ConfigParser({
        "url": "https://api.wunderground.com/api/",
        "country": "CZ",
        "city": "Prague"})
    config.read(config_file)

    wunderground_api = config.get('wunderground', 'api')
    wunderground_url = config.get('wunderground', 'url')
    country = config.get('wunderground', 'country')
    cities = config.get('wunderground', 'cities').split(",")
    print("api:", wunderground_api)
    print("url:", wunderground_url)
    print(len(cities))
    print(cities)

    while True:
        for city in cities:
            print("---------")
            print("Location:", city)
            response = influx.get_data(get_url(wunderground_url,
                                               wunderground_api,
                                               country,
                                               city),
                                       "")
            tags = {"location": city, "source": "wunderground"}

            print(str(response).encode('utf-8'))

            influx.parse_result(response, tags)

        time.sleep(300)

    sys.exit(0)
